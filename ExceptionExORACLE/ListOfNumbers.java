import java.io.*;
import java.util.Vector;

public class ListOfNumbers {

    private Vector victor;
    private static final int size = 10;

    public ListOfNumbers() {
        victor = new Vector(size);
        for (int i = 0; i < size; i++)
            victor.addElement(new Integer(i));
        this.readList("InputFile.txt");
        this.writeList();
    }

    public void readList(String fileName) {
        String input = null;
        // String fileName = "InputFile.txt";
        try {
            RandomAccessFile rd = new RandomAccessFile(fileName, "r");
            while ((input = rd.readLine()) != null) {
                Integer x = Integer.parseInt(input);
                // not printing
                System.out.println(Integer.valueOf(x));
                victor.add(x);
            }
        } catch (FileNotFoundException e) {
            e.getMessage();
        } catch (IOException e) {
            e.getMessage();
        }

    }

    public static void cat(File named) throws IOException {
        RandomAccessFile input = null;
        String line = null;

        try {
            input = new RandomAccessFile(named, "r");
            while ((line = input.readLine()) != null) {
                System.out.println(line);
            }
            return;
        } catch (FileNotFoundException e) {
            e.getMessage();
        } catch (IOException e) {
            e.getMessage();
        } finally {
            if (input != null) {
                input.close();
            }
        }
    }

    public void writeList() {
        PrintStream out = null;

        try {
            System.out.println("Entering try statement");
            out = new PrintStream(new FileOutputStream("OutFile.txt"));

            for (int i = 0; i < size; i++)
                out.println("Value at: " + i + " = " + victor.elementAt(i));
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("Caught ArrayIndexOutOfBoundsException: " +
                    e.getMessage());
        } catch (IOException e) {
            System.err.println("Caught IOException: " + e.getMessage());
        } finally {
            if (out != null) {
                System.out.println("Closing PrintStream");
                out.close();
            } else {
                System.out.println("PrintStream not open");
            }
        }
    }

    public static void main(String[] args) {
        new ListOfNumbers();

    }
}